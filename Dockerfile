FROM node:alpine

RUN mkdir -p /srv/backend
WORKDIR /srv/backend

COPY ./ .

RUN yarn
RUN yarn build

EXPOSE $PORT

CMD yarn start
