# REST-API Backend

## Development setup
### Enviroment variables
Add to `.env` file

Choose values by preference, it only affects local environment
* `PORT: number` - defaults to 8080

### Development
* Add corresponding plugin for [editorconfig](https://editorconfig.org) to your IDE

### Execution
* `yarn dev` - starts server in watch mode
