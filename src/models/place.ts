import { MultiPolygon, Polygon } from "./polygon";
import { Rule } from "./rule";

export interface Place extends PlaceInfo {
  rules?: Rule[];
}

export interface MinPlace {
  id: string;
  name: string;
  type: PlaceType;
  trend: Trend;
  example: boolean;
}

export type PlaceType = "landkreis" | "bundesland" | "staat";
export type Trend = -1 | 0 | 1;

export interface PlaceInfo extends MinPlace {
  incidence: number;
  website: string;
}

export interface RestrictedPlace {
  placeId: string;
  denyingRules: Rule[];
  polygon?: Polygon | MultiPolygon;
}

export type PlaceId =
  | "bb"
  | "bw"
  | "by"
  | "be"
  | "hb"
  | "hh"
  | "he"
  | "mv"
  | "ns"
  | "nrw"
  | "rp"
  | "sl"
  | "sy"
  | "sa"
  | "sh"
  | "th";
