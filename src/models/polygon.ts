import { Coordinate } from "./routing";

export type OpenMapPolyline = Coordinate[];
export type OpenMapPolygon = OpenMapPolyline[];

export interface Polygon {
  type: "Polygon";
  coordinates: OpenMapPolyline[];
}

export interface MultiPolygon {
  type: "MultiPolygon";
  coordinates: OpenMapPolygon[];
}
