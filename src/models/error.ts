export class NotFoundError extends Error {}
export class WrongTypeDefinitionError extends Error {}
export class ResponseCodeError extends Error {}
