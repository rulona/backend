import { RestrictedPlace } from "./place";

export interface RoutingResponse {
  restrictedPlaces: RestrictedPlace[];
  route: Polyline[] | string[];
  routeBoundary: RouteBoundary;
}

export interface RouteBoundary {
  northeast: Coordinate;
  southwest: Coordinate;
}

type Polyline = Coordinate[];

export interface Coordinate {
  lat: number;
  lng: number;
}

export interface RoutingRequest {
  origin: string | number[];
  destination: string | number[];
  returnType?: "decoded" | "encoded";
}
