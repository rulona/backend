export enum Restriction {
  DriveThroughRestricted = 0,
  DriveThroughForbidden = 1,
  StayRestricted = 2,
  StayForbidden = 3,
  OvernightStayRestricted = 4,
  OvernightStayForbidden = 5,
}
