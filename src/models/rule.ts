import { Restriction } from "./restriction";

export interface Rule {
  id: number;
  categoryId: number;
  status: Status;
  text: string;
  restriction?: Restriction;
  timestamp: string;
}

export type Status = -1 | 0 | 1 | 2;
