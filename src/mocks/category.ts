import { Category } from "../models/category";

export const categories: Category[] = [
  {
    id: 0,
    name: "Einreisebeschränkungen",
  },
  {
    id: 1,
    name: "Kontaktbeschränkungen",
  },
  {
    id: 2,
    name: "Abstandsregeln",
  },
  {
    id: 3,
    name: "Hygieneregeln / Landesverordnungen",
  },
  {
    id: 4,
    name: "Einzelhandel",
  },
  {
    id: 5,
    name: "Gottesdienste",
  },
  {
    id: 6,
    name: "Fitness und Breitensport",
  },
  {
    id: 7,
    name: "Wellness, Kosmetik und Körperpflege",
  },
  {
    id: 8,
    name: "Private Hochzeits-, Geburtstags- oder sonstige Familienfeiern",
  },
  {
    id: 9,
    name: "Restaurants, Cafés und Gaststätten (indoor)",
  },
  {
    id: 10,
    name: "Biergärten und Außengastronomie",
  },
  {
    id: 11,
    name: "Bars, Pubs und Kneipen",
  },
  {
    id: 12,
    name: "Discotheken und Clubs",
  },
  {
    id: 13,
    name: "Hotels, Pensionen, Jugendherbergen",
  },
  {
    id: 14,
    name: "Ferienwohnungen und Ferienhäuser",
  },
  {
    id: 15,
    name: "Privatunterkünfte",
  },
  {
    id: 16,
    name: "Camping- und Wohnmobilstellplätze",
  },
  {
    id: 17,
    name: "Verkehr (inkl. Binnenschifffahrt und Seilbahn)",
  },
  {
    id: 18,
    name: "Busreisen",
  },
  {
    id: 19,
    name: "Airports",
  },
  {
    id: 20,
    name: "Großveranstaltungen und Events (Kultur und Sport)",
  },
  {
    id: 21,
    name: "Messen und Kongresse (öffentlich)",
  },
  {
    id: 22,
    name: "Tagungen und Meetings (geschlossener Teilnehmerkreis)",
  },
  {
    id: 23,
    name: "Kleinkunst und Kultur",
  },
  {
    id: 24,
    name: "Freizeitaktivitäten (outdoor)",
  },
  {
    id: 25,
    name: "Freizeitaktivitäten (indoor)",
  },
  {
    id: 26,
    name: "Geführte Touren und Aktivitäten",
  },
  {
    id: 27,
    name: "Freibäder",
  },
  {
    id: 28,
    name: "Freizeitbäder",
  },
  {
    id: 29,
    name: "Zoos / Tierparks / Botanische Gärten",
  },
  {
    id: 30,
    name: "Freizeitparks",
  },
  {
    id: 31,
    name: "Theater und Oper",
  },
  {
    id: 32,
    name: "Museen und Ausstellungen",
  },
  {
    id: 33,
    name: "Kinos",
  },
];
