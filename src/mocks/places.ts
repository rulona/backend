import { Place } from "../models/place";
import { Restriction } from "../models/restriction";
import { Rule } from "../models/rule";

const DRIVE_THROUGH_RESTRICTION: Omit<Rule, "id"> = {
  categoryId: 0,
  status: 0,
  text:
    "Aufgrund der neusten Verordnungen ist die Durchfahrt von Personen aus anderen Bundesländern verboten",
  timestamp: "20210607",
  restriction: Restriction.DriveThroughRestricted,
};

const DRIVE_THROUGH_FORBIDDEN: Omit<Rule, "id"> = {
  categoryId: 0,
  status: 0,
  text:
    "Aufgrund der neusten Verordnungen ist die Durchfahrt von Personen aus anderen Landkreisen verboten",
  timestamp: "20210607",
  restriction: Restriction.DriveThroughForbidden,
};

const STAY_RESTRICTED: Omit<Rule, "id"> = {
  categoryId: 0,
  status: 0,
  text:
    "Aufgrund der neusten Verordnungen ist der Aufenthalt von durchreisenden Personen aus anderen Bundesländern verboten. Diese können jedoch ohne Aufenthalt diesen Landkreis durchqueren.",
  timestamp: "20210607",
  restriction: Restriction.StayRestricted,
};

const STAY_FORBIDDEN: Omit<Rule, "id"> = {
  categoryId: 0,
  status: 0,
  text:
    "Aufgrund der neusten Verordnungen ist der Aufenthalt von durchreisenden Personen aus anderen Landkreisen verboten. Diese können jedoch ohne Aufenthalt diesen Landkreis durchqueren.",
  timestamp: "20210607",
  restriction: Restriction.StayForbidden,
};

const OVERNIGHT_STAY_RESTRICTED: Omit<Rule, "id"> = {
  categoryId: 0,
  status: 0,
  text:
    "Aufgrund der neusten Verordnungen ist Personen aus anderen Bundesländern verboten in diesem Landkreis zu übernachten. Diese können sich jedoch ohne Übernachtung in diesen Landkreis aufhalten.",
  timestamp: "20210607",
  restriction: Restriction.OvernightStayRestricted,
};

const OVERNIGHT_STAY_FORBIDDEN: Omit<Rule, "id"> = {
  categoryId: 0,
  status: 0,
  text:
    "Aufgrund der neusten Verordnungen ist Personen aus anderen Landkreisen verboten in diesem Landkreis zu übernachten. Diese können sich jedoch ohne Übernachtung in diesen Landkreis aufhalten.",
  timestamp: "20210607",
  restriction: Restriction.OvernightStayForbidden,
};

export const places: Place[] = [
  {
    id: "BAR",
    name: "Barnim",
    type: "landkreis",
    incidence: 9.2,
    trend: -1,
    website: "https://www.barnim.de/",
    example: false,
    rules: [
      {
        ...DRIVE_THROUGH_RESTRICTION,
        id: 0,
      },
    ],
  },
  {
    id: "LDS",
    name: "Dahme-Spreewald",
    type: "landkreis",
    incidence: 17,
    trend: 1,
    website: "https://www.dahme-spreewald.info/de/start",
    example: false,
    rules: [
      {
        ...DRIVE_THROUGH_RESTRICTION,
        id: 0,
      },
    ],
  },
  {
    id: "EE",
    name: "Elbe-Elster",
    type: "landkreis",
    incidence: 20.6,
    trend: -1,
    website: "https://www.lkee.de/",
    example: false,
    rules: [
      {
        ...DRIVE_THROUGH_FORBIDDEN,
        id: 0,
      },
    ],
  },
  {
    id: "HVL",
    name: "Havelland",
    type: "landkreis",
    incidence: 23.9,
    trend: 1,
    website: "https://www.havelland.de/",
    example: false,
    rules: [
      {
        ...DRIVE_THROUGH_FORBIDDEN,
        id: 0,
      },
    ],
  },
  {
    id: "MOL",
    name: "Märkisch-Oderland",
    type: "landkreis",
    incidence: 8.2,
    trend: 1,
    website: "https://www.maerkisch-oderland.de/de/startseite.html",
    example: false,
    rules: [
      {
        ...STAY_RESTRICTED,
        id: 0,
      },
    ],
  },
  {
    id: "OHV",
    name: "Oberhavel",
    type: "landkreis",
    incidence: 15.5,
    trend: -1,
    website: "https://www.oberhavel.de/",
    example: false,
    rules: [
      {
        ...STAY_RESTRICTED,
        id: 0,
      },
    ],
  },
  {
    id: "OSL",
    name: "Oberspreewald-Lausitz",
    type: "landkreis",
    incidence: 17.4,
    trend: -1,
    website: "https://www.osl-online.de/",
    example: false,
    rules: [
      {
        ...DRIVE_THROUGH_RESTRICTION,
        id: 0,
      },
    ],
  },
  {
    id: "LOS",
    name: "Oder-Spree",
    type: "landkreis",
    incidence: 8.4,
    trend: -1,
    website: "https://www.landkreis-oder-spree.de/",
    example: false,
    rules: [
      {
        ...STAY_RESTRICTED,
        id: 0,
      },
    ],
  },
  {
    id: "OPR",
    name: "Ostprignitz-Ruppin",
    type: "landkreis",
    incidence: 5.1,
    trend: 0,
    website: "https://www.ostprignitz-ruppin.de/",
    example: false,
  },
  {
    id: "PM",
    name: "Potsdam-Mittelmark",
    type: "landkreis",
    incidence: 17.1,
    trend: 1,
    website: "https://www.potsdam-mittelmark.de/de/startseite/",
    example: false,
    rules: [
      {
        ...STAY_RESTRICTED,
        id: 0,
      },
    ],
  },
  {
    id: "PR",
    name: "Prignitz",
    type: "landkreis",
    incidence: 7.9,
    trend: 1,
    website: "https://www.landkreis-prignitz.de/",
    example: false,
    rules: [
      {
        ...DRIVE_THROUGH_FORBIDDEN,
        id: 0,
      },
    ],
  },
  {
    id: "SPN",
    name: "Spree-Neiße",
    type: "landkreis",
    incidence: 14.1,
    trend: -1,
    website: "https://www.lkspn.de/",
    example: false,
    rules: [
      {
        ...STAY_FORBIDDEN,
        id: 0,
      },
    ],
  },
  {
    id: "TF",
    name: "Teltow-Fläming",
    type: "landkreis",
    incidence: 24.1,
    trend: 1,
    website: "https://www.teltow-flaeming.de/de/startseite.php",
    example: false,
  },
  {
    id: "UM",
    name: "Uckermark",
    type: "landkreis",
    incidence: 4.2,
    trend: -1,
    website: "https://www.uckermark.de/",
    example: false,
    rules: [
      {
        ...DRIVE_THROUGH_RESTRICTION,
        id: 0,
      },
    ],
  },
  {
    id: "BRB",
    name: "Brandenburg an der Havel",
    type: "landkreis",
    incidence: 8.3,
    trend: -1,
    website: "https://www.stadt-brandenburg.de/",
    example: true,
    rules: [
      {
        ...OVERNIGHT_STAY_RESTRICTED,
        id: 0,
      },
    ],
  },
  {
    id: "CB",
    name: "Cottbus",
    type: "landkreis",
    incidence: 11,
    trend: 0,
    website: "https://www.cottbus.de/",
    example: true,
    rules: [
      {
        ...OVERNIGHT_STAY_RESTRICTED,
        id: 0,
      },
    ],
  },
  {
    id: "FF",
    name: "Frankfurt (Oder)",
    type: "landkreis",
    incidence: 19,
    trend: 1,
    website: "https://www.frankfurt-oder.de/",
    example: false,
    rules: [
      {
        ...OVERNIGHT_STAY_FORBIDDEN,
        id: 0,
      },
    ],
  },
  {
    id: "P",
    name: "Potsdam",
    type: "landkreis",
    incidence: 16.1,
    trend: 1,
    website: "https://www.potsdam.de/",
    example: true,
    rules: [
      {
        ...OVERNIGHT_STAY_FORBIDDEN,
        id: 0,
      },
    ],
  },
  {
    id: "DE-BE",
    name: "Berlin",
    type: "bundesland",
    incidence: 26.3,
    trend: -1,
    website: "https://www.berlin.de/",
    example: true,
    rules: [],
  },
  {
    id: "BZ",
    name: "Bautzen",
    type: "landkreis",
    incidence: 39,
    trend: -1,
    website: "https://www.landkreis-bautzen.de/",
    example: false,
    rules: [
      {
        ...DRIVE_THROUGH_FORBIDDEN,
        id: 0,
      },
    ],
  },
  {
    id: "C",
    name: "Chemnitz",
    type: "landkreis",
    incidence: 25.2,
    trend: -1,
    website: "https://www.chemnitz.de/",
    example: false,
    rules: [
      {
        ...DRIVE_THROUGH_RESTRICTION,
        id: 0,
      },
    ],
  },
  {
    id: "DD",
    name: "Dresden",
    type: "landkreis",
    incidence: 23.9,
    trend: -1,
    website: "https://www.dresden.de/index_de.php",
    example: true,
    rules: [
      {
        ...DRIVE_THROUGH_RESTRICTION,
        id: 0,
      },
    ],
  },
  {
    id: "ERZ",
    name: "Erzgebirgskreis",
    type: "landkreis",
    incidence: 61.8,
    trend: -1,
    website: "https://www.erzgebirgskreis.de/de/",
    example: false,
    rules: [
      {
        ...DRIVE_THROUGH_RESTRICTION,
        id: 0,
      },
    ],
  },
  {
    id: "GR",
    name: "Görlitz",
    type: "landkreis",
    incidence: 28.5,
    trend: -1,
    website:
      "https://www.kreis-goerlitz.de/city_info/webaccessibility/index.cfm?waid=390",
    example: false,
  },
  {
    id: "L",
    name: "Leipzig",
    type: "landkreis",
    incidence: 17.4,
    trend: -1,
    website: "https://www.landkreisleipzig.de/",
    example: true,
  },
  {
    id: "MEI",
    name: "Meißen",
    type: "landkreis",
    incidence: 26.1,
    trend: -1,
    website: "http://www.kreis-meissen.org/",
    example: false,
    rules: [
      {
        ...DRIVE_THROUGH_RESTRICTION,
        id: 0,
      },
    ],
  },
  {
    id: "FG",
    name: "Mittelsachsen",
    type: "landkreis",
    incidence: 43.4,
    trend: -1,
    website: "https://www.landkreis-mittelsachsen.de/",
    example: false,
    rules: [
      {
        ...STAY_FORBIDDEN,
        id: 0,
      },
    ],
  },
  {
    id: "TDO",
    name: "Nordsachsen",
    type: "landkreis",
    incidence: 18.2,
    trend: -1,
    website: "https://www.landkreis-nordsachsen.de/",
    example: false,
    rules: [
      {
        ...STAY_FORBIDDEN,
        id: 0,
      },
    ],
  },
  {
    id: "PIR",
    name: "Sächsische Schweiz-Osterzgebirge",
    type: "landkreis",
    incidence: 37.9,
    trend: -1,
    website: "https://www.landratsamt-pirna.de/",
    example: false,
  },
  {
    id: "V",
    name: "Vogtlandkreis",
    type: "landkreis",
    incidence: 8,
    trend: -1,
    website: "https://www.vogtlandkreis.de/",
    example: false,
    rules: [
      {
        ...STAY_RESTRICTED,
        id: 0,
      },
    ],
  },
  {
    id: "Z",
    name: "Zwickau",
    type: "landkreis",
    incidence: 34,
    trend: -1,
    website: "https://www.landkreis-zwickau.de/",
    example: false,
    rules: [
      {
        ...STAY_RESTRICTED,
        id: 0,
      },
    ],
  },
  {
    id: "DE-BB",
    name: "Brandenburg",
    type: "bundesland",
    incidence: 14.1,
    trend: -1,
    website: "https://www.brandenburg.de",
    rules: [
      {
        id: 0,
        categoryId: 0,
        status: 1,
        text:
          "Nach der [Bundeseinreiseverordnung vom 12. Mai](https://www.bundesregierung.de/breg-de/themen/coronavirus/coronavirus-einreiseverordnung-1913466) müssen Personen, die per Flugzeug einreisen, vor Abflug einen negativen Test vorlegen (entfällt für Geimpfte und Genesene). Alle Personen, die aus einem Risikogebiet kommen (sich zu einem beliebigen Zeitpunkt in den letzten zehn Tagen vor der Einreise dort aufgehalten haben), müssen sich unter [www.einreiseanmeldung.de](http://www.einreiseanmeldung.de/) anmelden (digitale Einreiseanmeldung) und für zehn Tage absondern (bei Einreise aus einem Virusvariantengebiet 14 Tage). Geimpfte (mit einem in der EU zugelassenen Vakzin) und Genesene (max. 6 Monate zurückliegend) brauchen bei Einreisen keinen Test mehr und müssen auch nicht mehr in Quarantäne.\r\n\r\nMenschen, die nicht geimpft oder genesen sind, können sich bei Rückkehr freitesten, wenn sie aus einem Risikogebiet einreisen (gilt nicht bei Einreise aus einem Einreise in einem Virusvariantengebiet). Dafür reicht ein Antigentest, der nicht älter als 48 Stunden ist oder ein PCR-Test, der nicht älter als 72 Stunden ist.\r\n\r\nWeiterhin in Quarantäne müssen Reisende, die aus einem Hochinzidenzgebiet oder einem Virusvariantengebiet kommen. Wer aus einem Hochinzidenzgebiet kommt, kann sich ab dem fünften Tag der zehntägigen Quarantäne freitesten, bei einer Einreise aus dem Virusvariantengebiet ist das nicht möglich.\r\n\r\nAktuell vom RKI festgelegte [internationale Risikogebiete](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Risikogebiete_neu.html)",
        timestamp: "20210607",
      },
      {
        id: 1,
        categoryId: 1,
        status: 1,
        text:
          "Jede Person ist verpflichtet, die physischen Kontakte zu anderen Personen auf ein absolut nötiges Minimum zu reduzieren und den Personenkreis möglichst konstant zu halten.\r\n\r\nPrivate Zusammenkünfte mit Freunden, Verwandten und Bekannten sind nur mit den Angehörigen des eigenen Haushalts und mit Personen eines weiteren Haushalts gestattet (bei Überschreiten des 100er-Inzidenzwertes nur eine haushaltsfremde Person) oder mit bis zu insgesamt 10 Personen. Kinder unter 14 Jahren werden dabei nicht mitgezählt.\r\n\r\nFür Kreise und kreisfreie Städte gilt weiterhin eine klare Notbremse ab Überschreiten des 100er-Inzidenzwertes an drei aufeinanderfolgenden Tagen. In Landkreisen und kreisfreien Städten mit einer seit drei Tagen in Folge bestehenden 7-Tages-Inzidenz von 100 Neuinfektionen / 100.000 Einwohner gilt auf Grundlage der Änderungen des Infektionsschutzgesetzes vom 22.04.2021 (Bundesnotbremse) eine Ausgangssperre: der Aufenthalt außerhalb der eigenen Wohnung ist von 22 Uhr bis 5 Uhr des Folgetags untersagt. Ausnahmen sind dem Gesetz zu entnehmen. Zwischen 22 Uhr und 24 Uhr ist im Freien allein ausgeübte körperliche Bewegung erlaubt.\r\n\r\nPersonen, deren Testung mittels PCR-​Test ein positives Ergebnis ergeben hat, sind verpflichtet, das zuständige Gesundheitsamt zu informieren und bis zum Vorliegen einer Entscheidung sich unverzüglich auf direktem Weg in Quarantäne zu begeben. Personen, deren Testung mittels Schnelltest ein positives Ergebnis ergeben hat, sind verpflichtet, sich unverzüglich einem PCR-​Test zu unterziehen und sich bis zum Vorliegen des Testergebnisses unverzüglich in Quarantäne zu begeben.",
        timestamp: "20210607",
      },
      {
        id: 2,
        categoryId: 2,
        status: 1,
        text:
          "Ein Mindestabstand von 1,5 Metern ist zwischen Personen im öffentlichen und privaten Bereich grundsätzlich einzuhalten. sofern die Einhaltung des Mindestabstands nicht möglich ist, soll eine Mund-Nasen-Bedeckung getragen werden. Ausnahmen gibt es zum Beispiel für Ehe- oder Lebenspartner sowie Angehörige des eigenen Haushalts und in den Bereichen der Kindertagesbetreuung und der Jugendarbeit. Auch zwischen Schülerinnen und Schülern sowie zwischen Schülern und Lehrkräften gelten Ausnahmen.",
        timestamp: "20210607",
      },
      {
        id: 3,
        categoryId: 3,
        status: 1,
        text:
          "In allen öffentlich zugänglichen Gebäuden gilt in den für den Publikumsverkehr geöffneten Bereichen eine Pflicht zum Tragen einer medizinischen Maske. In Kraftfahrzeugen, die nicht dem öffentlichen Personenverkehr dienen, gilt für anwesende Personen mit Ausnahme des Fahrzeugführers eine Pflicht zum Tragen einer medizinischen Maske.\r\n\r\nFalls eine medizinische Maske zu tragen ist, muss diese entweder\n1. den Anforderungen an eine CE-gekennzeichnete medizinische Gesichtsmaske mit der Norm DIN EN 14683:2019-10 (OP-Maske) entsprechen oder\n2. eine die europäische Norm EN 149:2001+A1:2009 erfüllende FFP2-Maske sein, die mit einer CE-Kennzeich-nung mit vierstelliger Nummer der notifizierten Stelle gekennzeichnet ist.\nAls einer FFP2-Maske nach Satz 1 Nummer 2 vergleichbar gelten auch Masken mit den Typbezeichnungen N95, P2, DS2 oder eine Corona-Pandemie-Atemschutzmaske (CPA), insbesondere KN95.\r\n\r\nJede Person ist verpflichtet,\r\n\r\ndie physischen Kontakte zu anderen Personen auf ein absolut nötiges Minimum zu reduzieren und den Personenkreis möglichst konstant zu halten,\ndie allgemeinen Hygieneregeln und -empfehlungen des Robert Koch-Instituts und der Bundeszentrale für gesundheitliche Aufklärung zur Vorbeugung von Infektionen (https://www.infektionsschutz.de/coronavirus.html) zu beachten, einschließlich des regelmäßigen Austauschs der Raumluft durch Frischluft in geschlossenen Räumen,\naußerhalb des privaten Raums grundsätzlich einen Mindestabstand von 1,5 Metern zu anderen Personen einzuhalten (Abstandsgebot); sofern die Einhaltung des Mindestabstands nicht möglich ist, soll eine Mund-Nasen-Bedeckung getragen werden.\r\n\r\nLink zur Corona-Verordnung Brandenburg: https://kkm.brandenburg.de/kkm/de/verordnungen/",
        timestamp: "20210607",
      },
      {
        id: 4,
        categoryId: 4,
        status: 1,
        text:
          "Eingeschränkt geöffnet.\r\n\r\nDer Einzelhandel kann unter Auflagen öffnen: Dokumentationspflicht personenbezogener Daten; Bis zu einer Verkaufsfläche von 800 Quadratmetern nur ein Kunde pro zehn Quadratmeter sowie für die darüber hinausgehende Verkaufsfläche ein Kunde pro 20 Quadratmeter zeitgleich (gilt bis 10. Juni).\nEinschränkungen gelten nicht für Betriebe der Lebensmittel- und Grundversorgung.\r\n\r\nAllgemein: Steuerung des Zutritts; medizinische Mund-Nasen-Bedeckung verpflichtend; es gelten die allgemeinen Kontakt- und Hygieneregeln; Hygienekonzept erforderlich.\r\n\r\nWeitere Ausnahmen siehe §8 der Verordnung.\r\n\r\nRegelmäßiger Austausch der Raumluft durch Frischluft, insbesondere durch Stoßlüftung über Fenster oder durch den Betrieb raumlufttechnischer Anlagen mit hohem Außenluftanteil;\r\n\r\nIn Landkreisen und kreisfreien Städten mit einer seit drei Tagen in Folge bestehenden 7-Tages-Inzidenz von 100 Neuinfektionen / 100.000 Einwohner gilt auf Grundlage der Änderungen des Infektionsschutzgesetzes vom 22.04.2021 (Bundesnotbremse) zusätzlich:\r\n\r\nAusschließlich Geschäfte des täglichen Bedarfs dürfen geöffnet bleiben, geschlossenen Einzelhandelsbetriebe dürfen Lieferservice, Abholangebote (Click & Collect) und Terminshopping (Click & Meet) anbieten. Hier gilt zusätzlich: Terminvergabe verpflichtend, Testpflicht, Dokumentationspflicht personenbezogener Daten\r\n\r\nBei einer dem Publikumsverkehr zugänglichen Gesamtfläche vonbis zu 800 Quadratmeter gilt: max. 1 Kunde pro 20 m²\r\n\r\nBei einer dem Publikumsverkehr zugänglichen Gesamtfläche von über 800 Quadratmeter gilt: auf den ersten 800 Quadratmetern max. 1 Kunde pro 20 m² und darüber hinaus max. 1 Kunde pro 40 m²\r\n\r\nAb einer 7-Tages-Inzidenz von 150 Neuinfektionen / 100.000 Einwohneran drei aufeinander folgenden Tagen: Click & Meet unzulässig.",
        timestamp: "20210607",
      },
      {
        id: 5,
        categoryId: 5,
        status: 1,
        text:
          "Gestattet.\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln; Steuerung des Zutritts; medizinische Mund-Nasen-Bedeckung verpflichtend (auch am Platz); Hygienekonzept erforderlich; Dokumentationspflicht personenbezogener Daten.\r\n\r\nBei Veranstaltungen in geschlossenen Räumen den regelmäßigen Austausch der Raumluft durch Frischluft, insbesondere durch Stoßlüftung über Fenster oder durch den Betrieb raumlufttechnischer Anlagen mit hohem Außenluftanteil; Gemeindegesang ist untersagt.\r\n\r\nVeranstaltungen oder Zusammenkünfte mit mehr als zehn Personen sind der zuständigen Behörde spätestens zwei Tage zuvor anzuzeigen (Ausnahmen durch Schutzkonzept möglich)",
        timestamp: "20210607",
      },
      {
        id: 6,
        categoryId: 6,
        status: 1,
        text:
          "Eingeschränkt zulässig.\r\n\r\nIn geschlossenen Anlagen: Kontaktsport mit höchstens 30 Sportausübenden; Corona-Test erforderlich; Steuerung des Zutritts; vorherige Terminbuchung; Nur für asymptomatische Kunden; Dokumentationspflicht personenbezogener Daten; medizinische Mund-Nasen-Bedeckung verpflichtend.\r\n\r\nUnter freiem Himmel: Nur für asymptomatische Kunden.\r\n\r\nDampfsaunen und Dampfbäder geschlossen.\r\n\r\nSieben-Tage-Inzidenz über 100: Der Sportbetrieb auf und in allen Sportanlagen ist untersagt. Dies gilt insbesondere für Gymnastik-, Turn- und Sporthallen, Fitnessstudios, Tanzstudios, Tanzschulen, Bolzplätze, Skateranlagen und vergleichbare Einrichtungen.\r\n\r\nGilt nicht für Sportanlagen unter freiem Himmel mit kontaktfreiem Sport mit bis zu zehn Personen in dokumentierten Gruppen. Kinder bis einschließlich 14 Jahre dürfen in Gruppen von bis zu 20 Personen unter freiem Himmel kontaktlosen Sport ausüben\r\n\r\nDie Nutzung von Umkleiden und anderen Aufenthaltsräumen oder Gemeinschaftseinrichtungen ist untersagt. Auf weitläufigen Außensportanlagen dürfen mehrere Personengruppen Sport ausüben, sofern die Betreiberin oder der Betreiber gewährleistet, dass den einzelnen Personengruppen eine Mindestfläche von 800 Quadratmetern zur Sportausübung zur alleinigen Nutzung zugewiesen wird.\r\n\r\nIn Landkreisen und kreisfreien Städten mit einer seit drei Tagen in Folge bestehenden 7-Tages-Inzidenz von 100 Neuinfektionen / 100.000 Einwohner gilt auf Grundlage der Änderungen des Infektionsschutzgesetzes vom 22.04.2021 (Bundesnotbremse): Individualsportarten sind allein, zu zweit oder mit den Angehörigen des eigenen Haushalts zulässig. Kinder bis einschließlich 14 Jahre dürfen in Gruppen von bis zu 5 Personen unter freiem Himmel kontaktlosen Sport ausüben. Für die Aufsichtsperson gilt eine Testpflicht. Fitnessstudios sind geschlossen",
        timestamp: "20210607",
      },
      {
        id: 7,
        categoryId: 7,
        status: 1,
        text:
          "Körpernahe Dienstleistungen wie zum Beispiel Kosmetik-, Tattoo- und Sonnenstudios können unter Auflagen öffnen. Friseurbetriebe können unter Auflagen öffnen. Wenn das Abstandsgebot nicht eingehalten werden kann: Dokumentationspflicht personenbezogener Daten; Es gelten die allgemeinen Kontakt- und Hygieneregeln; Steuerung des Zutritts; medizinische Mund-Nasen-Bedeckung verpflichtend; Hygienekonzept erforderlich; Regelmäßiger Austausch der Raumluft durch Frischluft, insbesondere durch Stoßlüftung über Fenster oder durch den Betrieb raumlufttechnischer Anlagen mit hohem Außenluftanteil.\r\n\r\nWenn die besondere Eigenart der Dienstleistung das Tragen einer medizinischen Maske nicht zulässt, ist ein negatives Testergebnis erforderlich. Test ist nicht erforderlich, wenn vollständiger Impfschutz (mindestens 14 Tage zurückliegende Impfung) vorhanden ist (Impfdokumentation erforderlich; Nur für asymptomatische Kunden.\r\n\r\nDienstleistungen im Gesundheitsbereich und sonstige helfende Berufe sind zulässig, soweit diese medizinisch, pflegerisch oder therapeutisch notwendige Leistungen erbringen. Saunen und Dampfbäder sind geschlossen.\r\n\r\nDampfsaunen und Dampfbäder geschlossen.\r\n\r\nIn Landkreisen und kreisfreien Städten mit einer seit drei Tagen in Folge bestehenden 7-Tages-Inzidenz von 100 Neuinfektionen / 100.000 Einwohner gilt auf Grundlage der Änderungen des Infektionsschutzgesetzes vom 22.04.2021 (Bundesnotbremse): körpernahe Dienstleistungen sind untersagt, ausgenommen sind Friseurdienstleistungen, Fußpflege und medizinisch notwendige Behandlungen, es gilt in jedem Fall eine Testpflicht",
        timestamp: "20210607",
      },
      {
        id: 8,
        categoryId: 8,
        status: 1,
        text:
          "Private Feiern und sonstige Zusammenkünfte im Familien-, Freundes- oder Bekanntenkreis aus besonderem Anlass, insbesondere Verlobungsfeiern, Polterabende, Hochzeitsfeiern, Jubiläen, Geburtstags-, Einweihungs-, Prüfungs- und Abschlussfeiern, im privaten Wohnraum und im zugehörigen befriedeten Besitztum oder in öffentlichen oder angemieteten Räumen sind unter freiem Himmel mit bis zu 70 und in geschlossenen Räumen mit bis zu 30 Personen zulässig. es gelten die allgemeinen Kontakt- und Hygieneregeln.\r\n\r\nPrivate Zusammenkünfte mit Freunden, Verwandten und Bekannten sind nur mit den Angehörigen des eigenen Haushalts und mit Personen eines weiteren Haushalts gestattet (bei Überschreiten des 100er-Inzidenzwertes nur eine haushaltsfremde Person) oder mit bis zu insgesamt 10 Personen. Kinder bis zum vollendeten 14. Lebensjahr bleiben bei der Berechnung der Personenzahl unberücksichtigt.",
        timestamp: "20210607",
      },
      {
        id: 9,
        categoryId: 9,
        status: 1,
        text:
          "Eingeschränkt geöffnet.\r\n\r\nHöchstens zwei Haushalte an einem Tisch, mehr, wenn Abstandsgebote eingehalten werden kann; Einhaltung des Abstandsgebots zwischen den Gästen unterschiedlicher Tische sowie in Wartesituationen.\nSteuerung des Zutritts; Nur für asymptomatische Kunden; Corona-Test erforderlich; Dokumentation personenbezogener Daten;\nIn Landkreisen und kreisfreien Städten mit einer seit drei Tagen in Folge bestehenden 7-Tages-Inzidenz von 100 Neuinfektionen / 100.000 Einwohner gilt auf Grundlage der Änderungen des Infektionsschutzgesetzes vom 22.04.2021 (Bundesnotbremse): Der Abverkauf zum Mitnehmen ist zwischen 22 Uhr und 5 Uhr untersagt; die Auslieferung von Speisen und Getränken bleibt zulässig.",
        timestamp: "20210607",
      },
      {
        id: 10,
        categoryId: 9,
        status: 1,
        text:
          "Eingeschränkt geöffnet.\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln; Steuerung des Zutriits; medizinische Mund-Nasen-Bedeckung verpflichtend; Hygienekonzept erforderlich.",
        timestamp: "20210607",
      },
      {
        id: 11,
        categoryId: 9,
        status: -1,
        text:
          "In den Innenbereichen von Gaststätten haben die Betreiberinnen und Betreiber darüber hinaus den regelmäßigen Austausch der Raumluft durch Frischluft sicherzustellen, insbesondere durch Stoßlüftung über Fenster oder durch den Betrieb raumlufttechnischer Anlagen mit hohem Außenluftanteil; bei einem aus technischen oder technologischen Gründen nicht vermeidbaren Umluftbetrieb raumlufttechnischer Anlagen sollen diese über eine geeignete Filtration zur Abscheidung luftgetragener Viren verfügen.",
        timestamp: "20210607",
      },
      {
        id: 12,
        categoryId: 10,
        status: 1,
        text:
          "Eingeschränkt geöffnet.\r\n\r\nHöchstens zwei Haushalte an einem Tisch, mehr, wenn Abstandsgebote eingehalten werden kann; Einhaltung des Abstandsgebots zwischen den Gästen unterschiedlicher Tische sowie in Wartesituationen.\nSteuerung des Zutritts; Nur für asymptomatische Kunden; Corona-Test erforderlich; Dokumentation personenbezogener Daten;\nIn Landkreisen und kreisfreien Städten mit einer seit drei Tagen in Folge bestehenden 7-Tages-Inzidenz von 100 Neuinfektionen / 100.000 Einwohner gilt auf Grundlage der Änderungen des Infektionsschutzgesetzes vom 22.04.2021 (Bundesnotbremse): Der Abverkauf zum Mitnehmen ist zwischen 22 Uhr und 5 Uhr untersagt; die Auslieferung von Speisen und Getränken bleibt zulässig.",
        timestamp: "20210607",
      },
      {
        id: 13,
        categoryId: 10,
        status: 1,
        text:
          "Eingeschränkt geöffnet.\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln; Steuerung des Zutriits; medizinische Mund-Nasen-Bedeckung verpflichtend; Hygienekonzept erforderlich.",
        timestamp: "20210607",
      },
      {
        id: 14,
        categoryId: 11,
        status: 1,
        text:
          "Eingeschränkt geöffnet.\r\n\r\nHöchstens zwei Haushalte an einem Tisch, mehr, wenn Abstandsgebote eingehalten werden kann; Einhaltung des Abstandsgebots zwischen den Gästen unterschiedlicher Tische sowie in Wartesituationen.\nSteuerung des Zutritts; Nur für asymptomatische Kunden; Corona-Test erforderlich; Dokumentation personenbezogener Daten;\nIn Landkreisen und kreisfreien Städten mit einer seit drei Tagen in Folge bestehenden 7-Tages-Inzidenz von 100 Neuinfektionen / 100.000 Einwohner gilt auf Grundlage der Änderungen des Infektionsschutzgesetzes vom 22.04.2021 (Bundesnotbremse): Der Abverkauf zum Mitnehmen ist zwischen 22 Uhr und 5 Uhr untersagt; die Auslieferung von Speisen und Getränken bleibt zulässig.",
        timestamp: "20210607",
      },
      {
        id: 15,
        categoryId: 11,
        status: 1,
        text:
          "Eingeschränkt geöffnet.\nEs gelten die allgemeinen Kontakt- und Hygieneregeln; Steuerung des Zutriits; medizinische Mund-Nasen-Bedeckung verpflichtend; Hygienekonzept erforderlich.",
        timestamp: "20210607",
      },
      {
        id: 16,
        categoryId: 11,
        status: -1,
        text:
          "In den Innenbereichen von Gaststätten haben die Betreiberinnen und Betreiber darüber hinaus den regelmäßigen Austausch der Raumluft durch Frischluft sicherzustellen, insbesondere durch Stoßlüftung über Fenster oder durch den Betrieb raumlufttechnischer Anlagen mit hohem Außenluftanteil; bei einem aus technischen oder technologischen Gründen nicht vermeidbaren Umluftbetrieb raumlufttechnischer Anlagen sollen diese über eine geeignete Filtration zur Abscheidung luftgetragener Viren verfügen.",
        timestamp: "20210607",
      },
      {
        id: 17,
        categoryId: 12,
        status: 0,
        text: `Geschlossen.\r\n\r\nVorgaben für Außenbereiche siehe "Biergärten und Außengastronomie".`,
        timestamp: "20210607",
      },
      {
        id: 18,
        categoryId: 13,
        status: 1,
        text:
          "Eingeschränkt geöffnet; Es ist bis zum 10. Juni untersagt, Personen zu touristischen Zwecken zu beherbergen.\nÜbernachtungsangebote gegen Entgelt dürfen unabhängig von der Betriebsform nur zu geschäftlichen oder dienstlichen Zwecken zur Verfügung gestellt werden.\nDokumentationspflicht personenbezogener Daten.",
        timestamp: "20210607",
      },
      {
        id: 19,
        categoryId: 13,
        status: 1,
        text:
          "Eingeschränkt.\r\n\r\nSteuerung des Zutritts; medizinische Mund-Nasen-Bedeckung verpflichtend (in gemeinschaftlich genutzten Räumen); es gelten die allgemeinen Kontakt- und Hygieneregeln; Hygienekonzept erforderlich; erhöhte Hygienemaßnahmen.",
        timestamp: "20210607",
      },
      {
        id: 20,
        categoryId: 14,
        status: 1,
        text:
          "Eingeschränkt geöffnet.\r\n\r\nTouristische Beherbergungen möglich. In einer Unterkunft nur Angehörige aus höchstens zwei Haushalten gemeinsam.\r\n\r\nNur für asymptomatische Kunden; Corona-Test erforderlich vor Beginn des Aufenthalts und alle 72 Stunden (gilt nicht für Geschäftsreisen und zwingend erforderliche Reisen); Steuerung des Zutritts; Dokumentationspflicht personenbezogener Daten.\r\n\r\nSieben-Tage-Inzidenz über 100: Eingeschränkt geöffnet; Es ist untersagt, Personen zu touristischen Zwecken zu beherbergen.\nÜbernachtungsangebote gegen Entgelt dürfen unabhängig von der Betriebsform nur zu geschäftlichen oder dienstlichen Zwecken zur Verfügung gestellt werden.\nDokumentationspflicht personenbezogener Daten.",
        timestamp: "20210607",
      },
      {
        id: 21,
        categoryId: 14,
        status: 1,
        text:
          "Eingeschränkt.\r\n\r\nMedizinische Mund-Nasen-Bedeckung verpflichtend (in gemeinschaftlich genutzten Räumen); es gelten die allgemeinen Kontakt- und Hygieneregeln; Hygienekonzept erforderlich; erhöhte Hygienemaßnahmen.",
        timestamp: "20210607",
      },
      {
        id: 22,
        categoryId: 15,
        status: 1,
        text:
          "Eingeschränkt geöffnet.\r\n\r\nTouristische Beherbergungen möglich. In einer Unterkunft nur Angehörige aus höchstens zwei Haushalten gemeinsam.\r\n\r\nNur für asymptomatische Kunden; Corona-Test erforderlich vor Beginn des Aufenthalts und alle 72 Stunden (gilt nicht für Geschäftsreisen und zwingend erforderliche Reisen); Steuerung des Zutritts; Dokumentationspflicht personenbezogener Daten.\r\n\r\nSieben-Tage-Inzidenz über 100: Eingeschränkt geöffnet; Es ist untersagt, Personen zu touristischen Zwecken zu beherbergen.\nÜbernachtungsangebote gegen Entgelt dürfen unabhängig von der Betriebsform nur zu geschäftlichen oder dienstlichen Zwecken zur Verfügung gestellt werden.\nDokumentationspflicht personenbezogener Daten.",
        timestamp: "20210607",
      },
      {
        id: 23,
        categoryId: 15,
        status: 1,
        text:
          "Eingeschränkt.\nSteuerung des Zutritts; medizinische Mund-Nasen-Bedeckung verpflichtend (in gemeinschaftlich genutzten Räumen); es gelten die allgemeinen Kontakt- und Hygieneregeln; Hygienekonzept erforderlich; erhöhte Hygienemaßnahmen.",
        timestamp: "20210607",
      },
      {
        id: 24,
        categoryId: 16,
        status: 1,
        text:
          "Eingeschränkt geöffnet.\r\n\r\nTouristische Beherbergungen möglich. In einer Unterkunft nur Angehörige aus höchstens zwei Haushalten gemeinsam.\r\n\r\nNur für asymptomatische Kunden; Corona-Test erforderlich vor Beginn des Aufenthalts und alle 72 Stunden (gilt nicht für Geschäftsreisen und zwingend erforderliche Reisen); Steuerung des Zutritts; Dokumentationspflicht personenbezogener Daten.\r\n\r\nSieben-Tage-Inzidenz über 100: Eingeschränkt geöffnet; Es ist untersagt, Personen zu touristischen Zwecken zu beherbergen.\nÜbernachtungsangebote gegen Entgelt dürfen unabhängig von der Betriebsform nur zu geschäftlichen oder dienstlichen Zwecken zur Verfügung gestellt werden.\nDokumentationspflicht personenbezogener Daten.",
        timestamp: "20210607",
      },
      {
        id: 25,
        categoryId: 16,
        status: 1,
        text:
          "Eingeschränkt.\r\n\r\nMedizinische Mund-Nasen-Bedeckung verpflichtend (in gemeinschaftlich genutzten Räumen); es gelten die allgemeinen Kontakt- und Hygieneregeln; Hygienekonzept erforderlich; erhöhte Hygienemaßnahmen.",
        timestamp: "20210607",
      },
      {
        id: 26,
        categoryId: 17,
        status: 2,
        text:
          "Öffentlicher Verkehr ist gestattet.\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln.\nStadtrundfahrten, Schiffsausflüge und vergleichbare touristische Angebote sind untersagt.",
        timestamp: "20210607",
      },
      {
        id: 27,
        categoryId: 17,
        status: 1,
        text:
          "Eingeschränkt.\r\n\r\nMedizinische Mund-Nasen-Bedeckung verpflichtend (auch in den für den Publikumsverkehr zugänglichen Gebäuden von Bahnhöfen und Verkehrsflughäfen sowie in den zugehörigen Bereichen (insbesondere Wartebereiche, Haltestellen, Bahnsteige und Bahnhofsvorplätze); es gelten die allgemeinen Kontakt- und Hygieneregeln.\r\n\r\nIn Kraftfahrzeugen, die nicht dem öffentlichen Personenverkehr dienen, gilt für anwesende Personen mit Ausnahme des Fahrzeugführers eine Pflicht zum Tragen einer medizinischen Maske.",
        timestamp: "20210607",
      },
      {
        id: 28,
        categoryId: 18,
        status: 1,
        text:
          "Eingeschränkt zugelassen.\r\n\r\nReisebusreisen, Stadtrundfahrten, Schiffsausflüge und vergleichbare touristische Angebote möglich. Nur mit feste Sitzplätzen\nNur für asymptomatische Kunden; Corona-Test erforderlich.\r\n\r\nSieben-Tage-Inzidenz über 100: Reisebusreisen, Stadtrundfahrten, Schiffsausflüge und vergleichbare touristische Angebote sind untersagt.",
        timestamp: "20210607",
      },
      {
        id: 29,
        categoryId: 18,
        status: 1,
        text:
          "Eingeschränkt zugelassen.\r\n\r\nFahrgäste sowie das Fahrpersonal müssen bei direktem Gästekontakt eine medizinische Maske tragen; es gelten die allgemeinen Kontakt- und Hygieneregeln; Hygienekonzept erforderlich; erhöhte Hygienemaßnahmen.",
        timestamp: "20210607",
      },
      {
        id: 30,
        categoryId: 19,
        status: 2,
        text:
          "Geöffnet.\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln.",
        timestamp: "20210607",
      },
      {
        id: 31,
        categoryId: 19,
        status: 1,
        text:
          "Medizinische Mund-Nasen-Bedeckung verpflichtend (auch in den für den Publikumsverkehr zugänglichen Gebäuden von Bahnhöfen und Verkehrsflughäfen sowie in den zugehörigen Bereichen (insbesondere Wartebereiche, Haltestellen, Bahnsteige und Bahnhofsvorplätze); es gelten die allgemeinen Kontakt- und Hygieneregeln.",
        timestamp: "20210607",
      },
      {
        id: 32,
        categoryId: 20,
        status: 1,
        text:
          "Eingeschränkt zulässig.\r\n\r\nMit Unterhaltungscharakter: Unter freiem Himmel mit bis zu 500 und in geschlossenen Räumen mit bis zu 200 Personen.\nOhne Unterhaltungscharakter: Hygienekonzept erforderlich.\r\n\r\nVorherige Terminvergabe erforderlich; Nur für asymptomatische Kunden; Corona-Test erforderlich; Dokumentation personenbezogener Daten;\r\n\r\nMessen, Ausstellungen, Spezialmärkte, Jahrmärkte, Volksfeste sind bis zum 10.06.2021 untersagt.",
        timestamp: "20210607",
      },
      {
        id: 33,
        categoryId: 20,
        status: 1,
        text:
          "Eingeschränkt zulässig.\r\n\r\nSteuerung des Zutritts; Medizinische Mund-Nasen-Bedeckung verpflichtend (auch auf Wochenmärkten, einschließlich der Wege und Flächen zwischen den einzelnen Marktständen); es gelten die Abstandsregeln; Hygienekonzept erforderlich; erhöhte Hygienemaßnahmen.",
        timestamp: "20210607",
      },
      {
        id: 34,
        categoryId: 20,
        status: -1,
        text:
          "Bei Veranstaltungen in geschlossenen Räumen den regelmäßigen Austausch der Raumluft durch Frischluft, insbesondere durch Stoßlüftung über Fenster oder durch den Betrieb raumlufttechnischer Anlagen mit hohem Außenluftanteil.",
        timestamp: "20210607",
      },
      {
        id: 35,
        categoryId: 21,
        status: 0,
        text:
          "Geschlossen.\r\n\r\nMessen, Ausstellungen, Spezialmärkte, Jahrmärkte, Volksfeste sind bis zum 10.06.2021 untersagt.",
        timestamp: "20210607",
      },
      {
        id: 36,
        categoryId: 22,
        status: 1,
        text:
          "Eingeschränkt zulässig.\r\n\r\nNur für asymptomatische Kunden; Dokumentation personenbezogener Daten.\r\n\r\nMessen, Ausstellungen, Spezialmärkte, Jahrmärkte, Volksfeste sind bis zum 10.06.2021 untersagt.",
        timestamp: "20210607",
      },
      {
        id: 37,
        categoryId: 22,
        status: 1,
        text:
          "Eingeschränkt zulässig.\r\n\r\nSteuerung des Zutritts; Medizinische Mund-Nasen-Bedeckung verpflichtend; es gelten die Abstandsregeln; Hygienekonzept erforderlich; erhöhte Hygienemaßnahmen.",
        timestamp: "20210607",
      },
      {
        id: 38,
        categoryId: 22,
        status: -1,
        text:
          "Bei Veranstaltungen in geschlossenen Räumen den regelmäßigen Austausch der Raumluft durch Frischluft, insbesondere durch Stoßlüftung über Fenster oder durch den Betrieb raumlufttechnischer Anlagen mit hohem Außenluftanteil.",
        timestamp: "20210607",
      },
      {
        id: 39,
        categoryId: 23,
        status: 1,
        text:
          "Eingeschränkt zulässig.\r\n\r\nUnter freiem Himmel mit bis zu 500 und in geschlossenen Räumen mit bis zu 200 Personen.\nVorherige Terminvergabe erforderlich; Nur für asymptomatische Kunden; Corona-Test erforderlich; Dokumentation personenbezogener Daten;\r\n\r\nMessen, Ausstellungen, Spezialmärkte, Jahrmärkte, Volksfeste sind bis zum 10.06.2021 untersagt.",
        timestamp: "20210607",
      },
      {
        id: 40,
        categoryId: 23,
        status: 1,
        text:
          "Eingeschränkt zulässig.\r\n\r\nSteuerung des Zutritts; Medizinische Mund-Nasen-Bedeckung verpflichtend (auch auf Wochenmärkten, einschließlich der Wege und Flächen zwischen den einzelnen Marktständen); es gelten die Abstandsregeln; Hygienekonzept erforderlich; erhöhte Hygienemaßnahmen.",
        timestamp: "20210607",
      },
      {
        id: 41,
        categoryId: 24,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nVorherige Terminvergabe erforderlich (es sei denn nur Außenflächen); Nur für asymptomatische Kunden; Steuerung des Zutritts; Dokumentationspflicht personenbezogener Daten.",
        timestamp: "20210607",
      },
      {
        id: 42,
        categoryId: 24,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln; Steuerung des Zutritts; medizinische Mund-Nasen-Bedeckung verpflichtend; Hygienekonzept erforderlich;",
        timestamp: "20210607",
      },
      {
        id: 43,
        categoryId: 25,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nVorherige Terminvergabe erforderlich (es sei denn nur Außenflächen); Nur für asymptomatische Kunden; Steuerung des Zutritts; Dokumentationspflicht personenbezogener Daten.",
        timestamp: "20210607",
      },
      {
        id: 44,
        categoryId: 25,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln; Steuerung des Zutritts; medizinische Mund-Nasen-Bedeckung verpflichtend; Hygienekonzept erforderlich;",
        timestamp: "20210607",
      },
      {
        id: 45,
        categoryId: 25,
        status: -1,
        text:
          "In geschlossenen Räumen einen regelmäßigen Austausch der Raumluft durch Frischluft, insbesondere durch Stoßlüftung über Fenster oder durch den Betrieb raumlufttechnischer Anlagen mit hohem Außenluftanteil; bei einem aus technischen oder technologischen Gründen nicht vermeidbaren Umluftbetrieb raumlufttechnischer Anlagen sollen diese über eine geeignete Filtration zur Abscheidung luftgetragener Viren verfügen.",
        timestamp: "20210607",
      },
      {
        id: 46,
        categoryId: 26,
        status: 1,
        text:
          "Eingeschränkt zugelassen.\r\n\r\nReisebusreisen, Stadtrundfahrten, Schiffsausflüge und vergleichbare touristische Angebote möglich. Nur mit feste Sitzplätzen\nNur für asymptomatische Kunden; Corona-Test erforderlich.\r\n\r\nSieben-Tage-Inzidenz über 100: Reisebusreisen, Stadtrundfahrten, Schiffsausflüge und vergleichbare touristische Angebote sind untersagt.",
        timestamp: "20210607",
      },
      {
        id: 47,
        categoryId: 26,
        status: 1,
        text:
          "Inzidenzabhängig eingeschränkt zugelassen.\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln; Medizinische Mund-Nasen-Bedeckung verpflichtend; Hygienekonzept erforderlich.",
        timestamp: "20210607",
      },
      {
        id: 48,
        categoryId: 27,
        status: 1,
        text:
          "Eingeschränkt geöffnet.\r\n\r\nUnter freiem Himmel mit bis zu 500 und in geschlossenen Räumen mit bis zu 200 Personen.\nSteuerung des Zutritts; Nur für asymptomatische Kunden; Dokumentationspflicht personenbezogener Daten;\nSieben-Tage-Inzidenz über 100: Geschlossen.",
        timestamp: "20210607",
      },
      {
        id: 49,
        categoryId: 27,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln; Steuerung des Zutritts; medizinische Mund-Nasen-Bedeckung verpflichtend (nicht im Außenbereich); Hygienekonzept erforderlich;",
        timestamp: "20210607",
      },
      {
        id: 50,
        categoryId: 28,
        status: 0,
        text: "Bis zum 10. Juni geschlossen.",
        timestamp: "20210607",
      },
      {
        id: 51,
        categoryId: 29,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nVorherige Terminvergabe erforderlich (es sei denn nur Außenflächen); Nur für asymptomatische Kunden; Steuerung des Zutritts; Dokumentationspflicht personenbezogener Daten.",
        timestamp: "20210607",
      },
      {
        id: 52,
        categoryId: 29,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln; Steuerung des Zutritts; medizinische Mund-Nasen-Bedeckung verpflichtend (nicht im Außenbereich); Hygienekonzept erforderlich;",
        timestamp: "20210607",
      },
      {
        id: 53,
        categoryId: 29,
        status: -1,
        text:
          "In geschlossenen Räumen einen regelmäßigen Austausch der Raumluft durch Frischluft, insbesondere durch Stoßlüftung über Fenster oder durch den Betrieb raumlufttechnischer Anlagen mit hohem Außenluftanteil; bei einem aus technischen oder technologischen Gründen nicht vermeidbaren Umluftbetrieb raumlufttechnischer Anlagen sollen diese über eine geeignete Filtration zur Abscheidung luftgetragener Viren verfügen.",
        timestamp: "20210607",
      },
      {
        id: 54,
        categoryId: 30,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nVorherige Terminvergabe erforderlich (es sei denn nur Außenflächen); Nur für asymptomatische Kunden; Steuerung des Zutritts; Dokumentationspflicht personenbezogener Daten.",
        timestamp: "20210607",
      },
      {
        id: 55,
        categoryId: 30,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln; Steuerung des Zutritts; medizinische Mund-Nasen-Bedeckung verpflichtend (nicht im Außenbereich); Hygienekonzept erforderlich;",
        timestamp: "20210607",
      },
      {
        id: 56,
        categoryId: 30,
        status: -1,
        text:
          "In geschlossenen Räumen einen regelmäßigen Austausch der Raumluft durch Frischluft, insbesondere durch Stoßlüftung über Fenster oder durch den Betrieb raumlufttechnischer Anlagen mit hohem Außenluftanteil; bei einem aus technischen oder technologischen Gründen nicht vermeidbaren Umluftbetrieb raumlufttechnischer Anlagen sollen diese über eine geeignete Filtration zur Abscheidung luftgetragener Viren verfügen.",
        timestamp: "20210607",
      },
      {
        id: 57,
        categoryId: 31,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nVorherige Terminvergabe erforderlich (es sei denn nur Außenflächen); Nur für asymptomatische Kunden; Steuerung des Zutritts; Dokumentationspflicht personenbezogener Daten.",
        timestamp: "20210607",
      },
      {
        id: 58,
        categoryId: 31,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln; Steuerung des Zutritts; medizinische Mund-Nasen-Bedeckung verpflichtend (nicht im Außenbereich); Hygienekonzept erforderlich;",
        timestamp: "20210607",
      },
      {
        id: 59,
        categoryId: 31,
        status: -1,
        text:
          "In geschlossenen Räumen einen regelmäßigen Austausch der Raumluft durch Frischluft, insbesondere durch Stoßlüftung über Fenster oder durch den Betrieb raumlufttechnischer Anlagen mit hohem Außenluftanteil; bei einem aus technischen oder technologischen Gründen nicht vermeidbaren Umluftbetrieb raumlufttechnischer Anlagen sollen diese über eine geeignete Filtration zur Abscheidung luftgetragener Viren verfügen.",
        timestamp: "20210607",
      },
      {
        id: 60,
        categoryId: 32,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nVorherige Terminvergabe erforderlich (es sei denn nur Außenflächen); Nur für asymptomatische Kunden; Steuerung des Zutritts; Dokumentationspflicht personenbezogener Daten.",
        timestamp: "20210607",
      },
      {
        id: 61,
        categoryId: 32,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln; Steuerung des Zutritts; medizinische Mund-Nasen-Bedeckung verpflichtend (nicht im Außenbereich); Hygienekonzept erforderlich;",
        timestamp: "20210607",
      },
      {
        id: 62,
        categoryId: 32,
        status: -1,
        text:
          "In geschlossenen Räumen einen regelmäßigen Austausch der Raumluft durch Frischluft, insbesondere durch Stoßlüftung über Fenster oder durch den Betrieb raumlufttechnischer Anlagen mit hohem Außenluftanteil; bei einem aus technischen oder technologischen Gründen nicht vermeidbaren Umluftbetrieb raumlufttechnischer Anlagen sollen diese über eine geeignete Filtration zur Abscheidung luftgetragener Viren verfügen.",
        timestamp: "20210607",
      },
      {
        id: 63,
        categoryId: 33,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nVorherige Terminvergabe erforderlich (es sei denn nur Außenflächen); Nur für asymptomatische Kunden; Steuerung des Zutritts; Dokumentationspflicht personenbezogener Daten.",
        timestamp: "20210607",
      },
      {
        id: 64,
        categoryId: 33,
        status: 1,
        text:
          "Eingeschränkt geöffnet. (geschlossen bei Überschreiten des 100er-Inzidenzwertes)\r\n\r\nEs gelten die allgemeinen Kontakt- und Hygieneregeln; Steuerung des Zutritts; medizinische Mund-Nasen-Bedeckung verpflichtend (nicht im Außenbereich); Hygienekonzept erforderlich;",
        timestamp: "20210607",
      },
      {
        id: 65,
        categoryId: 33,
        status: -1,
        text:
          "In geschlossenen Räumen einen regelmäßigen Austausch der Raumluft durch Frischluft, insbesondere durch Stoßlüftung über Fenster oder durch den Betrieb raumlufttechnischer Anlagen mit hohem Außenluftanteil; bei einem aus technischen oder technologischen Gründen nicht vermeidbaren Umluftbetrieb raumlufttechnischer Anlagen sollen diese über eine geeignete Filtration zur Abscheidung luftgetragener Viren verfügen.",
        timestamp: "20210607",
      },
      {
        ...OVERNIGHT_STAY_RESTRICTED,
        id: 66,
      },
    ],
    example: true,
  },
  {
    id: "DE-SY",
    name: "Sachsen",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://www.sachsen.de/",
    example: true,
    rules: [
      {
        ...OVERNIGHT_STAY_RESTRICTED,
        id: 0,
      },
    ],
  },
  {
    id: "DE-BW",
    name: "Baden Württemberg",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://www.baden-wuerttemberg.de/de/startseite/",
    example: true,
    rules: [],
  },
  {
    id: "DE-BY",
    name: "Bayern",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://www.bayern.de",
    example: true,
    rules: [],
  },
  {
    id: "DE-HB",
    name: "Bremen",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://www.bremen.de",
    example: true,
    rules: [],
  },
  {
    id: "DE-HH",
    name: "Hamburg",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://www.hamburg.de",
    example: true,
    rules: [],
  },
  {
    id: "DE-HE",
    name: "Hessen",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://www.hessen.de",
    example: true,
    rules: [],
  },
  {
    id: "DE-MV",
    name: "Mecklemburg Vorpommern",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://www.regierung-mv.de",
    example: true,
    rules: [],
  },
  {
    id: "DE-NS",
    name: "Niedersachsen",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://www.niedersachsen.de/startseite/",
    example: true,
    rules: [],
  },
  {
    id: "DE-NRW",
    name: "Nordrhein-Westfalen",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://www.land.nrw",
    example: true,
    rules: [],
  },
  {
    id: "DE-RP",
    name: "Rheinland-Pfalz",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://www.rlp.de/de/startseite/",
    example: true,
    rules: [],
  },
  {
    id: "DE-SL",
    name: "Saarland",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://www.saarland.de/DE/home/home_node.html",
    example: true,
    rules: [],
  },
  {
    id: "DE-SA",
    name: "Sachsen-Anhalt",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://www.sachsen-anhalt.de/startseite/",
    example: true,
    rules: [],
  },
  {
    id: "DE-SH",
    name: "Schleswig-Holstein",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://www.schleswig-holstein.de/DE/Home/home_node.html",
    example: true,
    rules: [],
  },
  {
    id: "DE-TH",
    name: "Thüringen",
    type: "bundesland",
    incidence: 29.4,
    trend: -1,
    website: "https://thueringen.de",
    example: true,
    rules: [],
  },
  {
    id: "DE",
    name: "Deutschland",
    type: "staat",
    incidence: 26,
    trend: -1,
    website: "https://www.bundesregierung.de/breg-de",
    example: true,
  },
];
