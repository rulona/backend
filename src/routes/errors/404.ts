import { Router, Request, Response, NextFunction } from "express";

const notFound = Router();

notFound.use("*", (_: Request, res: Response, __: NextFunction) => {
  res.status(404).send("Not found");
});

export default notFound;
