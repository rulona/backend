import { Router, Request, Response } from "express";
import { NotFoundError } from "../models/error";
import { validatePath } from "../services/pathValidity";
import { notFound } from "./errors";

const pathValidityRouter = Router();

pathValidityRouter.post("/", async (req: Request, res: Response) => {
  try {
    const restrictedPlaces = await validatePath(req.body.placeIds);
    res.status(200).send(restrictedPlaces);
  } catch (err) {
    console.error(err);
    if (err instanceof NotFoundError) {
      res.status(404).send(err.message);
    } else {
      res.status(500).send("Internal Server Error");
    }
  }
});

pathValidityRouter.use("*", notFound);

export default pathValidityRouter;
