import { Router, Request, Response } from "express";
import { NotFoundError } from "rxjs";
import { WrongTypeDefinitionError } from "../models/error";
import { route } from "../services/routing";
import { notFound } from "./errors";

const routingRouter = Router();

routingRouter.post("/", async (req: Request, res: Response) => {
  console.log(`[${new Date().toISOString()}] ${JSON.stringify(req.body)}`);
  try {
    const result = await route(req.body);
    res.status(200).send(result);
    console.log(
      `[${new Date().toISOString()}] ${JSON.stringify(req.body)} responded`
    );
  } catch (err) {
    console.error(err);
    if (err instanceof NotFoundError) {
      res.status(404).send(err.message);
    } else if (err instanceof WrongTypeDefinitionError) {
      res.status(400).send(err.message);
    } else {
      res.status(500).send("Internal Server Error");
    }
  }
});

routingRouter.use("*", notFound);

export default routingRouter;
