import { Router } from "express";
import { notFound } from "./errors";

const common = Router();

common.use("*", notFound);

export default common;
