import { Router, Request, Response } from "express";
import { getAllCategories } from "../services/categories";
import { notFound } from "./errors";

const categoriesRouter = Router();

categoriesRouter.get("/", async (req: Request, res: Response) => {
  try {
    const categories = await getAllCategories();
    res.status(200).send(categories);
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

categoriesRouter.use("*", notFound);

export default categoriesRouter;
