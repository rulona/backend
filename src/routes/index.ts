import { Router } from "express";
import categoriesRouter from "./categories";
import common from "./common";
import pathValidityRouter from "./pathValidity";
import placesRouter from "./places";
import routingRouter from "./routing";

const routes = Router();

routes.use("/places", placesRouter);
routes.use("/categories", categoriesRouter);
routes.use("/pathValidity", pathValidityRouter);
routes.use("/routing", routingRouter);
routes.use("/", common);

export default routes;
