import { Router, Request, Response } from "express";
import { notFound } from "./errors";
import {
  getAllPlaces,
  getPlaceInfo,
  getRulesByPlaceId,
  updatePlaces,
} from "../services/places";
import { NotFoundError } from "../models/error";

const placesRouter = Router();

/**
 * Returns a list of all Places
 */
placesRouter.get("/", async (req: Request, res: Response) => {
  try {
    const places = await getAllPlaces();
    res.status(200).send(places);
  } catch (err) {
    console.error(err);
    if (err instanceof NotFoundError) {
      res.status(404).send(err.message);
    } else {
      res.status(500).send("Internal Server Error");
    }
  }
});

placesRouter.get("/update", async (req: Request, res: Response) => {
  await updatePlaces();
  res.sendStatus(200);
});

/**
 * Returns the info of a place
 */
placesRouter.get("/:id", async (req: Request, res: Response) => {
  try {
    const placeInfo = await getPlaceInfo(req.params.id);
    res.status(200).send(placeInfo);
  } catch (err) {
    if (err instanceof NotFoundError) {
      res.status(404).send(err.message);
    } else {
      res.status(500).send("Internal Server Error");
    }
  }
});

placesRouter.get("/:id/rules", async (req: Request, res: Response) => {
  try {
    const rules = await getRulesByPlaceId(req.params.id);
    res.status(200).send(rules);
  } catch (err) {
    if (err instanceof NotFoundError) {
      res.status(404).send(err.message);
    } else {
      res.status(500).send("Internal Server Error");
    }
  }
});

placesRouter.use("*", notFound);

export default placesRouter;
