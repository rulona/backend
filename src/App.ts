import express from "express";
import middlewares from "./middleware";
import routes from "./routes";
import { updatePlacePolygons } from "./services/geolocation";

class App {
  public express: express.Application;

  constructor() {
    this.loadData();
    this.express = express();
    this.applyMiddleware();
    this.express.use("/", routes);
  }

  private applyMiddleware(): void {
    for (const f of middlewares) {
      f(this.express);
    }
  }

  private async loadData(): Promise<void> {
    await updatePlacePolygons();
  }
}

export default new App().express;
