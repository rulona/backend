import dotenv from "dotenv";

dotenv.config();

export const PORT = process.env.PORT || 8080;
export const OPEN_STREET_MAP_URL = "https://nominatim.openstreetmap.org";
export const GOOGLE_MAPS_URL = "https://maps.googleapis.com/maps/api";
export const USER_AGENT_HEADER = {
  "User-Agent": "Rulona App",
};
