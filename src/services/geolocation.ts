import { places } from "../mocks/places";
import axios from "axios";
import { OPEN_STREET_MAP_URL, USER_AGENT_HEADER } from "../constants";
import {
  MultiPolygon,
  OpenMapPolygon,
  OpenMapPolyline,
  Polygon,
} from "../models/polygon";
import { Place } from "../models/place";
import { RateLimiter } from "limiter";
import { Coordinate } from "../models/routing";
const limiter = new RateLimiter({ tokensPerInterval: 1, interval: 1100 });
const placePolygons = new Map<string, Polygon | MultiPolygon>();
const HEADERS = {
  ...USER_AGENT_HEADER,
};

export const updatePlacePolygons = async (): Promise<boolean> => {
  console.log("Load Polygons");
  for (const place of places) {
    try {
      console.log(`Get Polygon for place ${place.name}`);
      const polygon = await updatePlacePolygonByPlace(place);
      placePolygons.set(place.id, polygon);
    } catch (e) {
      console.log(e);
    }
  }
  console.log("Polygons loaded!");
  return true;
};

const updatePlacePolygonByPlace = async (
  place: Place
): Promise<Polygon | MultiPolygon> => {
  let query;
  switch (place.type) {
    case "landkreis":
      query = "county";
      break;
    case "bundesland":
      query = "state";
      break;
    default:
      query = "country";
  }
  const url = encodeURI(
    `${OPEN_STREET_MAP_URL}/search?${query}=${place.name}&format=json&polygon_geojson=1`
  );
  await limiter.removeTokens(1);
  const searchResponse = await axios.get(url, { headers: HEADERS });
  if (searchResponse.data.length > 0) {
    const searchedPlace = searchResponse.data[0];
    const geojson = searchedPlace.geojson;
    if (geojson.type === "Polygon") {
      const openMapPolylines: OpenMapPolyline[] = [];
      for (const polyline of geojson.coordinates) {
        const openMapCoordinates: Coordinate[] = [];
        for (const coordinate of polyline) {
          openMapCoordinates.push({ lat: coordinate[1], lng: coordinate[0] });
        }
        openMapPolylines.push(openMapCoordinates);
      }
      return {
        type: "Polygon",
        coordinates: openMapPolylines,
      };
    } else {
      const openMapPolygons: OpenMapPolygon[] = [];
      for (const polygon of geojson.coordinates) {
        const openMapPolylines: OpenMapPolyline[] = [];
        for (const polyline of polygon) {
          const openMapCoordinates: Coordinate[] = [];
          for (const coordinate of polyline) {
            openMapCoordinates.push({ lat: coordinate[1], lng: coordinate[0] });
          }
          openMapPolylines.push(openMapCoordinates);
        }
        openMapPolygons.push(openMapPolylines);
      }
      return {
        type: "MultiPolygon",
        coordinates: openMapPolygons,
      };
    }
  } else {
    throw `Search failed for: ${place.name}`;
  }
};

const updatePlacePolygonById = async (
  id: string
): Promise<Polygon | MultiPolygon> => {
  for (const place of places) {
    if (place.id === id) {
      return await updatePlacePolygonByPlace(place);
    }
  }
  throw `Unknown place id: ${id}`;
};

export const getPolygon = async (
  id: string
): Promise<Polygon | MultiPolygon> => {
  let result = placePolygons.get(id);
  if (!result) {
    result = await updatePlacePolygonById(id);
    placePolygons.set(id, result);
  }
  return result;
};
