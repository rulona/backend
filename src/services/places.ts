import { places } from "../mocks/places";
import { categories } from "../mocks/category";
import { NotFoundError } from "../models/error";
import { Place, MinPlace, PlaceInfo, PlaceId, Trend } from "../models/place";
import { Rule, Status } from "../models/rule";
import _ from "lodash";
import axios from "axios";
import { parse, HTMLElement, TextNode } from "node-html-parser";
import moment from "moment";
import { getId } from "./util";

// eslint-disable-next-line prettier/prettier
const minimizePlace = (place: Place): MinPlace => _.pick(place, ["id", "name", "type", "trend", "example"]);
const ID_NOT_FOUND_MSG = "Id could not be found";
const RULES_NOT_FOUND_MSG = "There are no rules for this place.";
const TOURISMUS_WEGWEISER_URL = "https://tourismus-wegweiser.de/detail/";
const INCIDENCE_STATIC_THRESHOLD = 0.5;
const RKI_BASE_URL = process.env.RKI_URL;
const RKI_URL = {
  STATES: `${RKI_BASE_URL}/states`,
  STATES_HISTORY: `${RKI_BASE_URL}/states/history/incidence`,
  DISTRICTS: `${RKI_BASE_URL}/districts`,
  DISTRICTS_HISTORY: `${RKI_BASE_URL}/districts/history/incidence`,
  GERMANY: `${RKI_BASE_URL}/germany`,
  GERMANY_HISTORY: `${RKI_BASE_URL}/germany/history/incidence`,
};
const ID = getId();
const PLACE_IDS = [
  "bb",
  "bw",
  "by",
  "be",
  "hb",
  "hh",
  "he",
  "mv",
  "ns",
  "nrw",
  "rp",
  "sl",
  "sy",
  "sa",
  "sh",
  "th",
];
const PLACE_ID_MAP_TO_RKI = {
  bb: "BB",
  bw: "BW",
  by: "BY",
  be: "BE",
  hb: "HB",
  hh: "HH",
  he: "HE",
  mv: "MV",
  ns: "NI",
  nrw: "NW",
  rp: "RP",
  sl: "SL",
  sy: "SN",
  sa: "ST",
  sh: "SH",
  th: "TH",
};

const parseIncidence = (inc: number): number => {
  return _.round(inc, 1);
};

const getTrend = (current: number, hist: number): Trend => {
  const difference = current - hist;
  if (difference < -INCIDENCE_STATIC_THRESHOLD) return -1;
  if (
    difference >= -INCIDENCE_STATIC_THRESHOLD &&
    difference <= INCIDENCE_STATIC_THRESHOLD
  )
    return 0;
  return 1;
};

export const getAllPlaces = async (): Promise<MinPlace[]> => {
  return places.map(minimizePlace);
};

export const getPlaceInfo = async (id: string): Promise<PlaceInfo> => {
  const place = places.find((place) => place.id === id);
  if (place) {
    return _.omit(place, ["rules"]);
  } else {
    throw new NotFoundError(`${ID_NOT_FOUND_MSG}: ${id}`);
  }
};

export const getRulesByPlaceId = async (id: string): Promise<Rule[]> => {
  const place = places.find((place) => place.id === id);
  if (!place) throw new NotFoundError(`${ID_NOT_FOUND_MSG}: ${id}`);
  if (!place.rules) throw new NotFoundError(RULES_NOT_FOUND_MSG);
  return place.rules;
};

export const updatePlaces = async (): Promise<boolean> => {
  const dateString = moment().format("YYYYMMDD");
  await updateIncidencesGermany();
  await updateIncidencesStates();
  await updateIncidencesDistricts();
  for (const placeId of PLACE_IDS) {
    const response = await axios.get(TOURISMUS_WEGWEISER_URL, {
      params: { bl: placeId, lang: "de" },
    });
    const root = parse(response.data);
    const contentTab1 = root.querySelector("#tab-1");
    const contentTab2 = root.querySelector("#tab-2");
    const table1 = contentTab1.querySelector("table");
    const table2 = contentTab2.querySelector("table");
    const table1Rules = getDataFromTable1(table1, dateString);
    const table2Rules = getDataFromTable2(table2, dateString);
    const rules = table1Rules.concat(table2Rules);
    for (const place of places) {
      if (place.id === `DE-${placeId.toUpperCase()}`) {
        place.rules = rules;
      }
    }
  }
  return true;
};

const updateIncidencesGermany = async (): Promise<void> => {
  const germanyResponse = await axios.get(RKI_URL.GERMANY);
  const germanyHistoryResponse = await axios.get(RKI_URL.GERMANY_HISTORY);
  const germany = germanyResponse.data;
  const germanyHistory = germanyHistoryResponse.data.data;
  const placeGer = places.filter((place) => place.id === "DE")[0];
  placeGer.incidence = parseIncidence(germany.weekIncidence);
  const histGer = germanyHistory[germanyHistory.length - 2].weekIncidence;
  placeGer.trend = getTrend(placeGer.incidence, histGer);
};

const updateIncidencesStates = async (): Promise<void> => {
  const stateResponse = await axios.get(RKI_URL.STATES);
  const stateHistoryResponse = await axios.get(RKI_URL.STATES_HISTORY);
  const states = stateResponse.data.data;
  const statesHistory = stateHistoryResponse.data.data;
  for (const id of PLACE_IDS) {
    places.forEach((place) => {
      if (place.id === `DE-${id.toUpperCase()}`) {
        place.incidence = parseIncidence(
          states[PLACE_ID_MAP_TO_RKI[id as PlaceId]].weekIncidence
        );
        const hist = statesHistory[PLACE_ID_MAP_TO_RKI[id as PlaceId]].history;
        const incidenceYesterday = hist[hist.length - 2].weekIncidence;
        place.trend = getTrend(place.incidence, incidenceYesterday);
      }
    });
  }
};

const updateIncidencesDistricts = async (): Promise<void> => {
  const districtResponse = await axios.get(RKI_URL.DISTRICTS);
  const districtHistoryResponse = await axios.get(RKI_URL.DISTRICTS_HISTORY);
  const districts = districtResponse.data.data;
  const districtsHistory = districtHistoryResponse.data.data;
  const newPlaces: Place[] = [];
  districtLoop: for (const key in districts) {
    const district = districts[key];
    let inc: number, trend: -1 | 0 | 1;
    if (districtsHistory[key] === undefined) {
      inc = district.weekIncidence;
      trend = 0;
    } else {
      const incTrend = getIncidenceAndTrend(
        district,
        districtsHistory[key].history
      );
      inc = incTrend.inc;
      trend = incTrend.trend;
    }
    for (const place of places) {
      if (district.name === place.name) {
        place.incidence = parseIncidence(inc);
        place.trend = trend;
        continue districtLoop;
      }
    }
    newPlaces.push({
      id: key,
      name: district.name,
      type: "landkreis",
      trend: trend,
      example: false,
      incidence: parseIncidence(inc),
      website: "",
      rules: [],
    });
  }
  for (const place of newPlaces) {
    places.push(place);
  }
};

const getIncidenceAndTrend = (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  district: any,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  hist: any
): { trend: -1 | 0 | 1; inc: number } => {
  const inc = district.weekIncidence;
  const histInc = hist[hist.length - 2];
  if (!histInc) return { inc, trend: 0 };
  const incidenceYesterday = hist[hist.length - 2].weekIncidence;
  const trend = getTrend(inc, incidenceYesterday);
  return { inc, trend };
};

const getDataFromTable1 = (table: HTMLElement, dateString: string): Rule[] => {
  const cn = table.querySelector("tbody").childNodes;
  const childnodes = cn.slice(1, cn.length - 1);
  const rules: Rule[] = [];
  let rule: Rule = createRule(-1, dateString, -1);
  for (let i = 0; i < childnodes.length; i++) {
    const td = childnodes[i] as HTMLElement;
    if (i % 2 === 0) {
      rule = createRule(extractCategoryId(td), dateString, -1);
    } else {
      rule.status = extractStatus(td);
      rule.text = extractText(td);
      rules.push(rule);
    }
  }
  return rules;
};

const getDataFromTable2 = (table: HTMLElement, dateString: string): Rule[] => {
  const cn = table.querySelector("tbody").childNodes;
  const childnodes = cn.slice(1, cn.length - 1);
  const rules: Rule[] = [];
  let rule: Rule = createRule(-1, dateString, -1);
  let categoryId = -1;
  for (let i = 0; i < childnodes.length; i++) {
    const td = childnodes[i] as HTMLElement;
    switch (i % 4) {
      case 0: {
        categoryId = extractCategoryId(td);
        break;
      }
      case 1:
      case 2: {
        rule = createRule(categoryId, dateString, -1);
        const status = extractStatus(td);
        if (rule.status === -1 || rule.status > status) rule.status = status;
        const text = extractText(td);
        rule.text = text.slice(1, text.length);
        rules.push(rule);
        break;
      }
      default: {
        rule = createRule(categoryId, dateString, rule.status);
        const text = getText(td.childNodes[0] as HTMLElement);
        rule.text = text.slice(1, text.length);
        if (rule.text.length > 0) {
          rules.push(rule);
        }
      }
    }
  }
  return rules;
};

const createRule = (
  categoryId: number,
  dateString: string,
  status: Status
): Rule => {
  return {
    id: ID.next().value,
    categoryId: categoryId,
    status: status,
    text: "",
    timestamp: dateString,
  };
};

const extractCategoryId = (categoryTd: HTMLElement): number => {
  const category = categoryTd.innerText;
  for (const c of categories) {
    if (c.name === category) return c.id;
  }
  return -1;
};

const extractStatus = (td: HTMLElement): -1 | 0 | 1 | 2 => {
  const statusNode = td.childNodes[0] as HTMLElement;
  const clazz = statusNode.attributes.class;
  if (clazz.includes("1")) return 2;
  if (clazz.includes("2")) return 1;
  if (clazz.includes("3")) return 0;
  return -1;
};

const extractText = (td: HTMLElement): string => {
  const textNode = td.childNodes[1] as HTMLElement;
  return getText(textNode);
};

const getText = (element: HTMLElement): string => {
  const childNodes = element.childNodes;
  let t = ``;
  for (const node of childNodes) {
    if (node instanceof TextNode) {
      t = `${t}${node.rawText}`;
    } else if (node instanceof HTMLElement) {
      if (node.rawTagName === "a") {
        t = `${t}${getLinkFromAnchor(node)}`;
      } else {
        t = `${t}${getText(node)}`;
      }
    }
  }
  return t;
};

const getLinkFromAnchor = (element: HTMLElement): string => {
  const text = element.innerHTML;
  const link = element.attributes.href;
  return `[${text}](${link})`;
};
