import { categories } from "../mocks/category";
import { Category } from "../models/category";

export const getAllCategories = async (): Promise<Category[]> => {
  return categories;
};
