import axios from "axios";
import { RateLimiter } from "limiter";
import { isNumber } from "lodash";
import { GOOGLE_MAPS_URL, USER_AGENT_HEADER } from "../constants";
import { places } from "../mocks/places";
import {
  NotFoundError,
  ResponseCodeError,
  WrongTypeDefinitionError,
} from "../models/error";
import { PlaceType } from "../models/place";
import { Coordinate, RoutingRequest, RoutingResponse } from "../models/routing";
import { validatePathWithPolygons } from "./pathValidity";

const limiter = new RateLimiter({ tokensPerInterval: 35, interval: 1000 });
const HEADERS = USER_AGENT_HEADER;

export const route = async (req: RoutingRequest): Promise<RoutingResponse> => {
  let url = `${GOOGLE_MAPS_URL}/directions/json?`;
  url = `${url}${buildDirectionsRequestString(req.origin, "origin")}&`;
  url = `${url}${buildDirectionsRequestString(req.destination, "destination")}`;
  url = `${url}&key=${process.env.MAPS_SERVICES_API_KEY}`;
  url = encodeURI(url);
  await limiter.removeTokens(1);
  const directionsResponse = await axios.get(url, { headers: HEADERS });
  if (directionsResponse.status !== 200) {
    throw new ResponseCodeError(
      `Failed to retrieve Data from Google Maps. Got Response code: ${directionsResponse.status}`
    );
  }
  const data = directionsResponse.data;
  if (data.routes.length === 0)
    throw new NotFoundError("A route could not be found.");
  const _route = data.routes[0];
  const steps = _route.legs[0].steps;
  const polylines = [];
  const locations: Coordinate[] = [];
  for (const step of steps) {
    if (req.returnType && req.returnType === "encoded") {
      polylines.push(step.polyline.points);
    } else {
      polylines.push(decodePolyline(step.polyline.points));
    }
    locations.push(step.start_location);
  }
  locations.push(steps[steps.length - 1].end_location);
  const placeIdsOnRoute = await getPlaceIdsByLocations(locations);
  const restrictedPlaces = await validatePathWithPolygons(placeIdsOnRoute);
  return {
    restrictedPlaces,
    route: polylines,
    routeBoundary: _route.bounds,
  };
};

const checkInput = (input: unknown): boolean => {
  return (
    Array.isArray(input) &&
    input.length === 2 &&
    isNumber(input[0]) &&
    isNumber(input[1])
  );
};

const buildDirectionsRequestString = (
  input: string | number[],
  label: string
): string => {
  if (typeof input === "string") {
    const place = places.find((place) => place.id === input);
    if (place) {
      return `${label}=${place.type} ${place.name}`;
    } else {
      throw new NotFoundError(`Id ${input} could not be found for ${label}`);
    }
  } else if (checkInput(input)) {
    return `${label}=${input[0]},${input[1]}`;
  }
  throw new WrongTypeDefinitionError(
    `Expected type of string | [number, number] but got ${typeof input}`
  );
};

const getPlaceIdsByLocations = async (
  locations: Coordinate[]
): Promise<string[]> => {
  const result = new Set<string>();
  for (const loc of locations) {
    const url = encodeURI(
      `${GOOGLE_MAPS_URL}/geocode/json?latlng=${loc.lat},${loc.lng}&result_type=administrative_area_level_3&language=de&key=${process.env.MAPS_SERVICES_API_KEY}`
    );
    await limiter.removeTokens(1);
    const response = await axios.get(url, { headers: HEADERS });
    const data = response.data;
    const results = data.results;
    if (results.length <= 0) continue;
    const addressComponents = results[0].address_components;
    if (addressComponents.length <= 0) continue;
    let countyToAdd = "";
    let stateToAdd = "";
    for (const ac of addressComponents) {
      if (ac.types.includes("administrative_area_level_3")) {
        // is landkreis
        countyToAdd = getPlaceIdByGoogleMapsName(ac.long_name, "landkreis");
      } else if (ac.types.includes("administrative_area_level_1")) {
        // is bundesland
        stateToAdd = getPlaceIdByGoogleMapsName(ac.long_name, "bundesland");
      }
    }
    if (stateToAdd.length > 0) result.add(stateToAdd);
    if (countyToAdd.length > 0) result.add(countyToAdd);
  }
  return Array.from(result);
};

const getPlaceIdByGoogleMapsName = (name: string, type: PlaceType): string => {
  let filtered = places.filter(
    (place) => place.type === type && place.name === name
  );
  if (filtered.length === 1) return filtered[0].id;
  filtered = places.filter(
    (place) =>
      place.type === type &&
      (name.includes(place.name) || place.name.includes(name))
  );
  if (filtered.length <= 0) return "";
  return filtered[0].id;
};

// https://gist.github.com/ismaels/6636986
const decodePolyline = (encoded: string): Coordinate[] => {
  const points: Coordinate[] = [];
  const len = encoded.length;
  let lat = 0,
    lng = 0,
    index = 0;
  while (index < len) {
    let b,
      shift = 0,
      result = 0;
    do {
      b = encoded.charAt(index++).charCodeAt(0) - 63;
      result |= (b & 0x1f) << shift;
      shift += 5;
    } while (b >= 0x20);
    const dlat = (result & 1) !== 0 ? ~(result >> 1) : result >> 1;
    lat += dlat;
    shift = 0;
    result = 0;
    do {
      b = encoded.charAt(index++).charCodeAt(0) - 63;
      result |= (b & 0x1f) << shift;
      shift += 5;
    } while (b >= 0x20);
    const dlng = (result & 1) !== 0 ? ~(result >> 1) : result >> 1;
    lng += dlng;
    points.push({ lat: lat / 1e5, lng: lng / 1e5 });
  }
  return points;
};
