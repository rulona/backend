import { places } from "../mocks/places";
import { NotFoundError } from "../models/error";
import { RestrictedPlace } from "../models/place";
import { Rule } from "../models/rule";
import { getPolygon } from "./geolocation";

const ID_NOT_FOUND_MSG = "Id could not be found";

const checkForDenyingRule = async (id: string): Promise<Rule[]> => {
  const place = places.find((place) => place.id === id);
  if (!place) throw new NotFoundError(`${ID_NOT_FOUND_MSG}: ${id}`);
  if (!place.rules) return [];
  return place.rules.filter((rule) => rule.restriction);
};

export const validatePath = async (
  placeIds: string[]
): Promise<RestrictedPlace[]> => {
  const restrictedPlaces: RestrictedPlace[] = [];
  for (const placeId of placeIds) {
    const denyingRules = await checkForDenyingRule(placeId);
    if (denyingRules.length > 0)
      restrictedPlaces.push({ placeId, denyingRules });
  }
  return restrictedPlaces;
};

export const validatePathWithPolygons = async (
  placeIds: string[]
): Promise<RestrictedPlace[]> => {
  const restrictedPlaces = await validatePath(placeIds);
  return Promise.all(
    restrictedPlaces.map(async (place) => ({
      ...place,
      polygon: await getPolygon(place.placeId),
    }))
  );
};
