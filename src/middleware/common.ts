import express, { Router } from "express";
import cors from "cors";

export const handleCors = (router: Router): void => {
  router.use(cors({ credentials: true, origin: true }));
};

export const handleBodyRequestParsing = (router: Router): void => {
  router.use(express.urlencoded({ extended: true }));
  router.use(express.json());
};
