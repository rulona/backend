import { PORT } from "./constants";
import http from "http";
import App from "./App";

App.set("port", PORT);
const server = http.createServer(App);
server.listen(PORT, () => {
  const addr = server.address();
  const bind = typeof addr === "string" ? `pipe ${addr}` : `port ${addr?.port}`;
  console.log(`Listening on ${bind}`);
});
